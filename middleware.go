package connratelimiter

import (
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/stefarf/automap"
)

const rejectMessageFormat = "[%s] Rejected: Connection rate limit"

type Limit struct {
	entries []time.Time
}

// Middleware creates an echo middleware to limit the rate of connection per IP address.
// The threshold is to determine when the internal map will be recreated to periodically release unused memory.
// Connection rate more than limitCount in limitDuration will be rejected. Try to use lower number for limitCount
// because the implementation will create slice with length equal to this number. So the lower number means lower memory
// footprint. The recreated callback is used for testing purpose. Set this with nil for normal operation.
func Middleware(
	threshold float32,
	limitCount int,
	limitDuration time.Duration,
	maxNumberOfIPAddresses int,
	recreated func(addrs []string),
) echo.MiddlewareFunc {
	am := automap.New[string, *Limit](threshold)

	if recreated != nil {
		am.Recreated = func(mActive map[string]*Limit) {
			var addrs []string
			for addr := range mActive {
				addrs = append(addrs, addr)
			}
			recreated(addrs)
		}
	}

	var mut sync.Mutex
	lastScan := time.Now()

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			now := time.Now()
			timeLimit := now.Add(-limitDuration)
			addr := getRemoteAddress(c.Request())

			{ // Scan
				mut.Lock()
				if lastScan.Before(timeLimit) {
					am.ForEach(func(k string, v *Limit) (done bool) {
						if len(v.entries) != 0 {
							lastEntry := v.entries[len(v.entries)-1]
							if lastEntry.Before(timeLimit) {
								am.Delete(k)
							}
						}
						return
					})
					lastScan = now
				}
				mut.Unlock()
			}

			{ // Limit
				mut.Lock()

				if l, ok := am.Get(addr); ok {
					l.entries = append(l.entries, now)
					if len(l.entries) > limitCount {
						l.entries = l.entries[len(l.entries)-limitCount:]
					}
					if len(l.entries) < limitCount || l.entries[0].Before(timeLimit) {
						mut.Unlock()
						return next(c)
					}
					mut.Unlock()
					return c.String(http.StatusServiceUnavailable, fmt.Sprintf(rejectMessageFormat, addr))
				}
				if am.Size() >= maxNumberOfIPAddresses {
					mut.Unlock()
					return c.String(http.StatusServiceUnavailable, fmt.Sprintf(rejectMessageFormat, addr))
				}

				l := &Limit{
					entries: []time.Time{now},
				}
				am.Set(addr, l)

				mut.Unlock()
			}

			return next(c)
		}
	}
}

func getRemoteAddress(r *http.Request) (addr string) {
	addr = r.Header.Get("Cf-Connecting-Ip")
	if addr != "" {
		return
	}
	addr = r.Header.Get("X-Forwarded-For")
	if addr != "" {
		return
	}
	addr = r.Header.Get("X-Real-Ip")
	if addr != "" {
		return
	}
	return r.RemoteAddr
}
