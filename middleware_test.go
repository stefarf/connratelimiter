package connratelimiter

import (
	"log"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"

	"github.com/labstack/echo/v4"
)

func TestMiddleware(t *testing.T) {
	limitCount := 100
	limitDuration := time.Millisecond * 100

	maxNumberOfIPAddresses := 3 // changing this value will require update to this spec

	var latestAddrs []string
	mapRecreated := false
	limiterMiddleware := Middleware(
		0.5,
		limitCount,
		limitDuration,
		maxNumberOfIPAddresses,
		func(addrs []string) {
			latestAddrs = addrs
			mapRecreated = true
		})

	type addrCode struct {
		addr string
		code int
	}
	var result struct {
		sync.Mutex
		nextMap map[string]int
		codeMap map[addrCode]int
	}
	result.nextMap = map[string]int{}
	result.codeMap = map[addrCode]int{}
	next := func(c echo.Context) error {
		result.Lock()
		defer result.Unlock()
		result.nextMap[c.Request().Header.Get("X-Forwarded-For")]++
		return nil
	}

	runLimiter := func(addr string) {
		r := httptest.NewRequest("GET", "/any/url", nil)
		r.Header.Set("X-Forwarded-For", addr)
		w := httptest.NewRecorder()
		e := echo.New()
		c := e.NewContext(r, w)

		f := limiterMiddleware(next)
		err := f(c)
		if err != nil {
			log.Fatal(err)
		}

		result.Lock()
		defer result.Unlock()
		result.codeMap[addrCode{addr, w.Code}]++
	}

	var wg sync.WaitGroup
	// Run the limiter concurrently, it will hit the limit only once
	for i := 0; i < limitCount; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			runLimiter("10.0.0.1")
		}()
	}
	// Run the limiter concurrently, it will NOT hit the limit
	for i := 0; i < limitCount-1; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			runLimiter("10.0.0.2")
		}()
	}
	wg.Wait()

	assertDeepEqual(t, result.nextMap, map[string]int{
		"10.0.0.1": limitCount - 1,
		"10.0.0.2": limitCount - 1,
	}, "nextMap")
	assertDeepEqual(t, result.codeMap, map[addrCode]int{
		addrCode{"10.0.0.1", http.StatusOK}:                 limitCount - 1,
		addrCode{"10.0.0.1", http.StatusServiceUnavailable}: 1,
		addrCode{"10.0.0.2", http.StatusOK}:                 limitCount - 1,
	}, "codeMap")

	time.Sleep(limitDuration / 2)
	runLimiter("10.0.0.3")
	time.Sleep(limitDuration / 2)
	runLimiter("10.0.0.4") // will trigger the table scanning to clean up the expired IP addresses

	// the map scanning will also trigger the table (map) to be recreated to release the memory

	if !mapRecreated {
		t.Error("expect map to be recreated")
	}
	// latestAddrs contains non-expired IP addresses
	assertDeepEqual(t, latestAddrs, []string{"10.0.0.3"}, "latestAddrs")

	assertDeepEqual(t, result.nextMap, map[string]int{
		"10.0.0.1": limitCount - 1,
		"10.0.0.2": limitCount - 1,
		"10.0.0.3": 1,
		"10.0.0.4": 1,
	}, "nextMap")
	assertDeepEqual(t, result.codeMap, map[addrCode]int{
		addrCode{"10.0.0.1", http.StatusOK}:                 limitCount - 1,
		addrCode{"10.0.0.1", http.StatusServiceUnavailable}: 1,
		addrCode{"10.0.0.2", http.StatusOK}:                 limitCount - 1,
		addrCode{"10.0.0.3", http.StatusOK}:                 1,
		addrCode{"10.0.0.4", http.StatusOK}:                 1,
	}, "codeMap")

	runLimiter("10.0.0.5")
	runLimiter("10.0.0.6") // will be rejected because maxNumberOfIPAddresses = 3
	runLimiter("10.0.0.7") // will be rejected because maxNumberOfIPAddresses = 3

	assertDeepEqual(t, result.nextMap, map[string]int{
		"10.0.0.1": limitCount - 1,
		"10.0.0.2": limitCount - 1,
		"10.0.0.3": 1,
		"10.0.0.4": 1,
		"10.0.0.5": 1,
	}, "nextMap")
	assertDeepEqual(t, result.codeMap, map[addrCode]int{
		addrCode{"10.0.0.1", http.StatusOK}:                 limitCount - 1,
		addrCode{"10.0.0.1", http.StatusServiceUnavailable}: 1,
		addrCode{"10.0.0.2", http.StatusOK}:                 limitCount - 1,
		addrCode{"10.0.0.3", http.StatusOK}:                 1,
		addrCode{"10.0.0.4", http.StatusOK}:                 1,
		addrCode{"10.0.0.5", http.StatusOK}:                 1,
		addrCode{"10.0.0.6", http.StatusServiceUnavailable}: 1,
		addrCode{"10.0.0.7", http.StatusServiceUnavailable}: 1,
	}, "codeMap")

	time.Sleep(limitDuration)

	// at this point, the table is clear so we can make connection again from 3 different IP addresses
	runLimiter("10.0.0.5")
	runLimiter("10.0.0.6")
	runLimiter("10.0.0.7")
	assertDeepEqual(t, result.nextMap, map[string]int{
		"10.0.0.1": limitCount - 1,
		"10.0.0.2": limitCount - 1,
		"10.0.0.3": 1,
		"10.0.0.4": 1,
		"10.0.0.5": 2,
		"10.0.0.6": 1,
		"10.0.0.7": 1,
	}, "nextMap")
	assertDeepEqual(t, result.codeMap, map[addrCode]int{
		addrCode{"10.0.0.1", http.StatusOK}:                 limitCount - 1,
		addrCode{"10.0.0.1", http.StatusServiceUnavailable}: 1,
		addrCode{"10.0.0.2", http.StatusOK}:                 limitCount - 1,
		addrCode{"10.0.0.3", http.StatusOK}:                 1,
		addrCode{"10.0.0.4", http.StatusOK}:                 1,
		addrCode{"10.0.0.5", http.StatusOK}:                 2,
		addrCode{"10.0.0.6", http.StatusServiceUnavailable}: 1,
		addrCode{"10.0.0.7", http.StatusServiceUnavailable}: 1,
		addrCode{"10.0.0.6", http.StatusOK}:                 1,
		addrCode{"10.0.0.7", http.StatusOK}:                 1,
	}, "codeMap")
}
